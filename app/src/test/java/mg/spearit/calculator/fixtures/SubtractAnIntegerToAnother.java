package mg.spearit.calculator.fixtures;

import mg.spearit.calculator.Calculator;

/**
 * Created by toky on 4/26/17.
 */
public class SubtractAnIntegerToAnother {
    private int intToBeSubtracted;
    private int minus;

    private Calculator calculator;

    public SubtractAnIntegerToAnother() {
        calculator = new Calculator();
    }

    public void setIntToBeSubtracted(int intToBeSubtracted) {
        this.intToBeSubtracted = intToBeSubtracted;
    }

    public void setMinus(int minus) {
        this.minus = minus;
    }

    public int resultOfSubtraction() {
        return calculator.minus(intToBeSubtracted, minus);
    }
}
