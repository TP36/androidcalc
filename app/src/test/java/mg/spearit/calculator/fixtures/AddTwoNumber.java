package mg.spearit.calculator.fixtures;

import mg.spearit.calculator.Calculator;

/**
 * Created by toky on 4/26/17.
 */
public class AddTwoNumber {

    private int first;
    private int second;

    private Calculator calculator;

    public AddTwoNumber() {
        calculator = new Calculator();
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int resultOfAddition() {
        return calculator.add(first, second);
    }
}
